package kg.amangram.weatherapp.data.model

import com.squareup.moshi.Json

data class CurrentWeather(
    val id: Int = 0,
    @Json(name = "dt")
    val date: Long = 0,
    val name: String = "",
    val weather: List<WeatherDetail> = emptyList(),
    val timezone: Int = 0,
    val main: Main = Main(),
    val coord: Coordinate = Coordinate()
)

data class Main(
    val temp: Double = 0.0
)

data class Coordinate(
    val lat: Double = 0.0,
    val lon: Double = 0.0
)