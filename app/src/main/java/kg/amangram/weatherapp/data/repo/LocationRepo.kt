package kg.amangram.weatherapp.data.repo

import kg.amangram.weatherapp.data.local.CitiesDao
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.CurrentWeather
import kg.amangram.weatherapp.data.remote.Api
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.toList

class LocationRepo(private val api: Api, private val db: CitiesDao) {

    suspend fun getSaved() = db.getSavedCities()

    suspend fun search(text: String) = api.searchCities(text)

    suspend fun saveCity(city: City) = db.add(city)

    suspend fun getCities() = flow<CurrentWeather> {
        db.getSavedCities().onEach {
            emit(api.getCurrentForecast(it.lat, it.lon))
        }
    }.flowOn(Dispatchers.IO)
        .toList()


}