package kg.amangram.weatherapp.data.model


import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import kg.amangram.weatherapp.data.local.Constants
import kotlinx.parcelize.Parcelize

@Entity(
    tableName = Constants.CITIES
)
@Keep
@Parcelize
data class City(
@PrimaryKey(autoGenerate = true)
val id: Int = 0,
val name: String="",
val country: String="",
val lat: Double = 0.0,
val lon: Double = 0.0
): Parcelable