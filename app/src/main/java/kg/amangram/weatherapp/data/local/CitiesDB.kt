package kg.amangram.weatherapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import kg.amangram.weatherapp.data.model.City

@Database(entities = [City::class], version = 1)
abstract class CitiesDB : RoomDatabase() {
    abstract fun getSaved(): CitiesDao
}