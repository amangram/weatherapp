package kg.amangram.weatherapp.data.local

object Constants {
    const val API_KEY_QUERY = "appid"
    const val CITY = "city"
    const val CITY_LIST = "city_list"
    const val POSITION = "position"
    const val CELSIUS = "°"
    const val API_KEY = "707bd4a094ad2f2a68b5bd549b4e1c41"
    const val BASE_URL = "https://api.openweathermap.org/"
    const val CITIES = "cities"
    const val DAY_BG = "https://static.wixstatic.com/media/e8cc3d_c2c3d853b1794359a75d5cf45b35924a~mv2.gif"
    const val NIGHT_BG =
        "https://64.media.tumblr.com/11ba95afb42a738d2b22c043206c744e/4d90facd676267c4-14/s500x750/863b38372bb626ad09b2f6e5d66ee3733391dcc5.gifv"
}