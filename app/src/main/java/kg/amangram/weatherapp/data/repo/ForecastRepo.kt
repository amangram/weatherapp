package kg.amangram.weatherapp.data.repo

import kg.amangram.weatherapp.data.remote.Api

class ForecastRepo(private val api: Api) {

   suspend fun getForecast(lat: Double,lon: Double)=api.getForecast(lat,lon)
}