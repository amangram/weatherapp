package kg.amangram.weatherapp.data.model

enum class WeatherState {
    THUNDERSTORM, DRIZZLE, RAIN, SNOW, ATMOSPHERE, CLEAR, CLOUDS
}