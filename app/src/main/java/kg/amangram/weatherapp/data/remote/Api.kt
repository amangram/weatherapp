package kg.amangram.weatherapp.data.remote

import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.CurrentWeather
import kg.amangram.weatherapp.data.model.ForecastResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("geo/1.0/direct?limit=10")
    suspend fun searchCities(@Query("q") text: String): Response<List<City>>

    @GET("data/2.5/onecall?exclude=minutely&units=metric")
    suspend fun getForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Response<ForecastResponse>

    @GET("data/2.5/weather?units=metric")
    suspend fun getCurrentForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ):CurrentWeather
}