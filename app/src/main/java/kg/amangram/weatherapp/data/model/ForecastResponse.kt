package kg.amangram.weatherapp.data.model

import com.squareup.moshi.Json


data class ForecastResponse(
    val current: Current,
    val timezone: String = "",
    val hourly: List<Hourly> = emptyList(),
    val daily: List<Daily> = emptyList()
)

data class Hourly(
    @Json(name = "dt")
    val date: Long = 0,
    var dateString: String = "",
    val temp: Double = 0.0,
    val weather: List<WeatherDetail> = emptyList()
)

data class Current(
    @Json(name = "dt")
    val date: Long = 0,
    val sunrise: Long = 0,
    val sunset: Long = 0,
    var dateString: String = "",
    val temp: Double = 0.0,
    @Json(name = "feels_like")
    val feelsLike: Double = 0.0,
    val pressure: Int = 0,
    val humidity: Int = 0,
    val uvi: Double = 0.0,
    val visibility: Int = 0,
    @Json(name = "wind_speed")
    val windSpeed: Double = 0.0,
    @Json(name = "wind_deg")
    val windDeg: Int = 0,
    val weather: List<WeatherDetail> = emptyList()
)

data class Daily(
    @Json(name = "dt")
    val date: Long = 0,
    var dateString: String = "",
    val temp: Temp = Temp(),
    val weather: List<WeatherDetail> = emptyList()
)

data class Temp(
    val min: Double = 0.0,
    val max: Double = 0.0
)

data class WeatherDetail(
    val id: Int = 0,
    val main: String = "",
    val description: String = "",
    val icon: String = ""
)