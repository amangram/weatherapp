package kg.amangram.weatherapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kg.amangram.weatherapp.data.model.City

@Dao
interface CitiesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(city: City)

    @Query("SELECT * FROM cities")
    suspend fun getSavedCities(): List<City>
}