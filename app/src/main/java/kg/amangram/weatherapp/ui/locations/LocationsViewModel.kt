package kg.amangram.weatherapp.ui.locations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.CurrentWeather
import kg.amangram.weatherapp.data.repo.LocationRepo
import kg.amangram.weatherapp.ui.base.BaseViewModel
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch

class LocationsViewModel(private val repo: LocationRepo) : BaseViewModel() {
    private val _citiesList = MutableLiveData<List<CurrentWeather>>()
    val citiesList: LiveData<List<CurrentWeather>>
        get() = _citiesList

    private var cities: List<City>? = null

    fun getSaved() {
        /*viewModelScope.launch {
            _citiesList.postValue(repo.getSaved())
        }*/
        viewModelScope.launch {
            _citiesList.postValue(repo.getCities())
            cities = repo.getSaved()
        }
    }

    fun navigateToSearch(){
        router.navigateTo(screens.searchScreen())
    }

    fun navigateToDetail(position: Int) {
        cities?.let { screens.forecastScreen(position, it) }?.let { router.navigateTo(it) }
    }

}