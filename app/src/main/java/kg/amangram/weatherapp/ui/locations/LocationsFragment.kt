package kg.amangram.weatherapp.ui.locations

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kg.amangram.weatherapp.R
import kg.amangram.weatherapp.databinding.LocationsFragmentBinding
import kg.amangram.weatherapp.ui.CityAdapter
import kg.amangram.weatherapp.ui.LocationAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

class LocationsFragment : Fragment(R.layout.locations_fragment) {

    class LocationsScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return LocationsFragment()
        }
    }

    private val viewModel: LocationsViewModel by viewModel()
    private var binding: LocationsFragmentBinding? = null

    private val cityAdapter: LocationAdapter by lazy {
        LocationAdapter {
            viewModel.navigateToDetail(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = LocationsFragmentBinding.bind(view)
        setAdapter()
        observe()
        viewModel.getSaved()
        binding?.fab?.setOnClickListener {
            viewModel.navigateToSearch()
        }
    }

    private fun observe() {
        viewModel.citiesList.observe(viewLifecycleOwner, { list ->
            if (list.isEmpty()) {
                Toast.makeText(requireContext(), getString(R.string.empty_db), Toast.LENGTH_SHORT)
                    .show()
            } else
                cityAdapter.swapData(list)
        })
    }

    private fun setAdapter() {
        val mLinearLayout = LinearLayoutManager(activity)
        binding?.rvCities?.apply {
            adapter = cityAdapter
            layoutManager = mLinearLayout
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

}