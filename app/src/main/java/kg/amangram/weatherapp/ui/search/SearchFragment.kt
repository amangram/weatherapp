package kg.amangram.weatherapp.ui.search

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kg.amangram.weatherapp.R
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.State
import kg.amangram.weatherapp.databinding.SearchFragmentBinding
import kg.amangram.weatherapp.gone
import kg.amangram.weatherapp.show
import kg.amangram.weatherapp.ui.CityAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

class SearchFragment : Fragment(R.layout.search_fragment), SearchView.OnQueryTextListener {

    class SearchScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return SearchFragment()
        }
    }

    private val viewModel: SearchViewModel by viewModel()
    private var binding: SearchFragmentBinding? = null

    private val cityAdapter: CityAdapter by lazy {
        CityAdapter {
            val dialog = AddCityDialog()
            dialog.arguments = bundleOf(Constants.CITY to it)
            dialog.show(childFragmentManager,dialog::class.java.simpleName)
            //viewModel.save(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = SearchFragmentBinding.bind(view)
        binding?.svCity?.setOnQueryTextListener(this)
        setAdapter()
        observe()
    }

    private fun observe() {
        viewModel.searchState.observe(viewLifecycleOwner, { state ->
            when (state) {
                is State.Loading -> {
                    binding?.pbSearch?.show()
                }
                is State.Success -> {
                    binding?.pbSearch?.gone()
                    cityAdapter.swapData(state.data)
                }
                is State.Failed -> {
                    binding?.pbSearch?.gone()
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setAdapter() {
        val mLinearLayout = LinearLayoutManager(activity)
        binding?.rvCities?.apply {
            adapter = cityAdapter
            layoutManager = mLinearLayout
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let {
            if (it.isNotBlank())
                viewModel.searchCity(it)
        }
        return true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}