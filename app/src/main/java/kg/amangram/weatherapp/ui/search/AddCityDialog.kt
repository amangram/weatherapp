package kg.amangram.weatherapp.ui.search

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import kg.amangram.weatherapp.*
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.State
import kg.amangram.weatherapp.databinding.DialogAddBinding
import kg.amangram.weatherapp.ui.HourAdapter
import kg.amangram.weatherapp.ui.detail.DayAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AddCityDialog : DialogFragment(R.layout.dialog_add) {

    private var binding: DialogAddBinding? = null
    private var city: City? = null
    private val viewModel: SearchViewModel by sharedViewModel()
    private val dayAdapter: DayAdapter by lazy {
        DayAdapter()
    }
    private val hourAdapter: HourAdapter by lazy {
        HourAdapter()
    }

    override fun getTheme() = R.style.DialogTheme

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        city = requireArguments().getParcelable(Constants.CITY)
        binding = DialogAddBinding.bind(view)
        setAdapter()
        city?.let { viewModel.getForecast(it) }
        observe()
        setClickListeners()
    }

    private fun setClickListeners() {
        binding?.tvCancel?.setOnClickListener {
            dismiss()
        }
        binding?.tvAdd?.setOnClickListener {
            city?.let { viewModel.save(it) }
            dismiss()
        }
    }

    private fun observe() {
        viewModel.forecast.observe(viewLifecycleOwner, { state ->
            when (state) {
                is State.Success -> {
                    setLoading(false)
                    val city = requireArguments().getParcelable<City>(Constants.CITY)
                    state.data.current.let { currentWeather ->
                        binding?.apply {
                            //forecastBackground.setGif(currentWeather.date.unixToHourInt(state.data.timezone))
                            forecastBackground.setBg(currentWeather.weather.first().id)
                            tvHeader.text =
                                city?.name?.formatSize(currentWeather.weather.first().main, 32)
                            state.data.daily.first().temp.let { maxMin ->
                                tvTemp.text =
                                    "${currentWeather.temp.toInt()}${Constants.CELSIUS}".formatSize(
                                        maxMin.max.formatMaxMin(maxMin.min),
                                        36
                                    )
                            }
                            tvSunrise.text = currentWeather.sunrise.unixToTime(state.data.timezone)
                            tvSunset.text = currentWeather.sunset.unixToTime(state.data.timezone)
                            tvFeelsLike.text =
                                currentWeather.feelsLike.toInt().toString() + Constants.CELSIUS
                            tvPressure.text =
                                currentWeather.pressure.toString() + getString(R.string.pressure_point)
                            tvWind.text =
                                "${currentWeather.windDeg.degreesToCardinal()} ${currentWeather.windSpeed} ${
                                    getString(R.string.wind_speed)
                                }"
                            tvHumidity.text = currentWeather.humidity.toString() + "%"
                            tvUvi.text = currentWeather.uvi.toString()
                            tvVisibility.text =
                                (currentWeather.visibility.toDouble() / 1000).toString() + getString(
                                    R.string.visibility_point
                                )
                        }
                    }
                    dayAdapter.swapData(state.data.daily)
                    hourAdapter.swapData(state.data.hourly)
                }
                is State.Loading -> setLoading(true)
                is State.Failed -> {
                    setLoading(false)
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setAdapter() {
        val hLinearLayout =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        val vLinearLayout = LinearLayoutManager(requireContext())
        binding?.rvDays?.apply {
            adapter = dayAdapter
            layoutManager = vLinearLayout
        }
        binding?.rvHourly?.apply {
            adapter = hourAdapter
            layoutManager = hLinearLayout
        }
    }

    private fun setLoading(isLoading: Boolean) {
        binding?.apply {
            content.isVisible = !isLoading
            appBar.isVisible = !isLoading
            loading.layoutLoading.isVisible = isLoading
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}