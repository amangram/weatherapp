package kg.amangram.weatherapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.Hourly
import kg.amangram.weatherapp.databinding.ItemCityBinding
import kg.amangram.weatherapp.databinding.ItemHourBinding
import kg.amangram.weatherapp.formatIconUrl

class HourAdapter :
    ListAdapter<Hourly, HourAdapter.HourVH>(HourDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourVH {
        val inflater = LayoutInflater.from(parent.context)
        return HourVH(ItemHourBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: HourVH, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapData(data: List<Hourly>) {
        submitList(data.toMutableList())
    }

    inner class HourVH(private val itemBinding: ItemHourBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: Hourly) {
            itemBinding.apply {
                tvTemp.text = item.temp.toInt().toString()+Constants.CELSIUS
                ivWeather.load(item.weather.first().icon.formatIconUrl())
                tvTime.text = item.dateString
            }
        }
    }

    private class HourDC : DiffUtil.ItemCallback<Hourly>() {
        override fun areItemsTheSame(
            oldItem: Hourly,
            newItem: Hourly
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: Hourly,
            newItem: Hourly
        ): Boolean {
            return oldItem.date == newItem.date
        }
    }
}