package kg.amangram.weatherapp.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.ui.detail.ForecastFragment
import java.util.ArrayList

class ForecastViewPagerAdapter(fragment: Fragment,val list: ArrayList<City>) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return list.size
    }

    override fun createFragment(position: Int): Fragment {
        return ForecastFragment.newInstance(list[position])
    }

}