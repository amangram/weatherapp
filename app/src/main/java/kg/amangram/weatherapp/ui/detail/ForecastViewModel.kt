package kg.amangram.weatherapp.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.ForecastResponse
import kg.amangram.weatherapp.data.model.State
import kg.amangram.weatherapp.data.repo.ForecastRepo
import kg.amangram.weatherapp.unixTo
import kg.amangram.weatherapp.unixToDay
import kg.amangram.weatherapp.unixToHour
import kotlinx.coroutines.launch
import org.json.JSONObject


class ForecastViewModel(private val city: City, private val repo: ForecastRepo) :
    ViewModel() {

    private val _forecast = MutableLiveData<State<ForecastResponse>>()
    val forecast: LiveData<State<ForecastResponse>>
        get() = _forecast

    init {
        getForecast()
    }

    fun getForecast() {
        viewModelScope.launch {
            _forecast.postValue(State.loading())
            val forecastResponse = repo.getForecast(city.lat, city.lon)
            if (forecastResponse.isSuccessful) {
                forecastResponse.body()?.apply {
                    current.dateString = current.date.unixToDay()
                    daily.map { dailyWeather ->
                        dailyWeather.dateString = dailyWeather.date.unixToDay()
                    }
                    hourly.mapIndexed { index, hourly ->
                        if (index==0)
                            hourly.dateString = "now"
                        else
                            hourly.dateString = hourly.date.unixToHour(timezone)
                    }
                    _forecast.postValue(State.success(this))
                }
            } else {
                try {
                    val jObjError = JSONObject(forecastResponse.errorBody()?.string())
                    _forecast.postValue(
                        State.failed(
                            jObjError.getString("Message")
                        )
                    )

                } catch (e: Exception) {
                }

            }
        }
    }
}