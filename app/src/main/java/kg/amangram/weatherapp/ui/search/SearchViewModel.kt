package kg.amangram.weatherapp.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.ForecastResponse
import kg.amangram.weatherapp.data.model.State
import kg.amangram.weatherapp.data.repo.ForecastRepo
import kg.amangram.weatherapp.data.repo.LocationRepo
import kg.amangram.weatherapp.ui.base.BaseViewModel
import kg.amangram.weatherapp.unixToDay
import kg.amangram.weatherapp.unixToHour
import kotlinx.coroutines.launch
import org.json.JSONObject

class SearchViewModel(private val repo: LocationRepo, private val forecastRepo: ForecastRepo) :
    BaseViewModel() {

    val searchState: LiveData<State<List<City>>>
        get() = _searchState
    private val _searchState = MutableLiveData<State<List<City>>>()
    private val _forecast = MutableLiveData<State<ForecastResponse>>()
    val forecast: LiveData<State<ForecastResponse>>
        get() = _forecast

    fun searchCity(text: String) {
        viewModelScope.launch {
            _searchState.postValue(State.loading())
            val response = repo.search(text)
            if (response.isSuccessful) {
                response.body()?.let {
                    _searchState.postValue(State.success(it))
                }
            } else {
                try {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    _searchState.postValue(
                        State.failed(
                            jObjError.getString("Message")
                        )
                    )
                } catch (e: Exception) {
                }
            }
        }
    }

    fun getForecast(city: City) {
        viewModelScope.launch {
            _forecast.postValue(State.loading())
            val forecastResponse = forecastRepo.getForecast(city.lat, city.lon)
            if (forecastResponse.isSuccessful) {
                forecastResponse.body()?.apply {
                    current.dateString = current.date.unixToDay()
                    daily.map { dailyWeather ->
                        dailyWeather.dateString = dailyWeather.date.unixToDay()
                    }
                    hourly.mapIndexed { index, hourly ->
                        if (index == 0)
                            hourly.dateString = "now"
                        else
                            hourly.dateString = hourly.date.unixToHour(timezone)
                    }
                    _forecast.postValue(State.success(this))
                }
            } else {
                try {
                    val jObjError = JSONObject(forecastResponse.errorBody()?.string())
                    _forecast.postValue(
                        State.failed(
                            jObjError.getString("Message")
                        )
                    )

                } catch (e: Exception) {
                }

            }
        }
    }

    fun save(city: City) {
        viewModelScope.launch {
            repo.saveCity(city)
        }
        //router.replaceScreen(screens.forecastScreen(-1))
    }
}