package kg.amangram.weatherapp.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator
import kg.amangram.weatherapp.R
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.City
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

class ForecastViewPagerFragment : Fragment(R.layout.fragment_forecast_view_pager) {

    private var viewPager: ViewPager2? = null
    private var pagerAdapter: ForecastViewPagerAdapter? = null

    class ForecastScreen(val position: Int,val list: List<City>) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return ForecastViewPagerFragment().apply {
                arguments = bundleOf(Constants.POSITION to position,Constants.CITY_LIST to list)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager = view.findViewById(R.id.viewPager)
        val springDotsIndicator = view.findViewById<SpringDotsIndicator>(R.id.spring_dots_indicator)
        val list = requireArguments().getParcelableArrayList<City>(Constants.CITY_LIST)
        pagerAdapter = list?.let { ForecastViewPagerAdapter(this, it) }
        viewPager?.adapter = pagerAdapter
        val position = requireArguments().getInt(Constants.POSITION)
        viewPager?.setCurrentItem(position,false)
        viewPager?.let {
            springDotsIndicator.setViewPager2(it)
        }
    }
}