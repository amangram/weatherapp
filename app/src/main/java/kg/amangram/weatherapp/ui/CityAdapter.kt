package kg.amangram.weatherapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.databinding.ItemCityBinding

class CityAdapter(private val interaction: (City) -> Unit) :
    ListAdapter<City, CityAdapter.CityVH>(CityDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityVH {
        val inflater = LayoutInflater.from(parent.context)
        return CityVH(ItemCityBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: CityVH, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapData(data: List<City>) {
        submitList(data.toMutableList())
    }

    inner class CityVH(private val itemBinding: ItemCityBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: City) {
            itemBinding.tvCity.text = "${item.name} , ${item.country}"
            itemBinding.root.setOnClickListener { interaction(item) }
        }
    }

    private class CityDC : DiffUtil.ItemCallback<City>() {
        override fun areItemsTheSame(
            oldItem: City,
            newItem: City
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: City,
            newItem: City
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }
}