package kg.amangram.weatherapp.ui.detail

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import coil.load
import kg.amangram.weatherapp.data.model.Daily
import kg.amangram.weatherapp.databinding.ItemDayBinding
import kg.amangram.weatherapp.formatIconUrl

class DayAdapter :
    ListAdapter<Daily, DayAdapter.DayVH>(DayDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayVH {
        val inflater = LayoutInflater.from(parent.context)
        return DayVH(ItemDayBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: DayVH, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapData(data: List<Daily>) {
        submitList(data.toMutableList())
    }

    inner class DayVH(private val itemBinding: ItemDayBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: Daily) {
            itemBinding.apply {
                tvDate.text = item.dateString
                tvDay.text = item.temp.max.toInt().toString()
                tvNight.text = item.temp.min.toInt().toString()
                ivWeather.load(item.weather.first().icon.formatIconUrl())
            }
            //val dayIcon = itemBinding.root.context.getIcon(item.day?.icon.toString())
            //val nightIcon = itemBinding.root.context.getIcon(item.night?.icon.toString())
            //itemBinding.ivDay.setImageResource(dayIcon)
            //itemBinding.ivNight.setImageResource(nightIcon)
        }
    }

    private class DayDC : DiffUtil.ItemCallback<Daily>() {
        override fun areItemsTheSame(
            oldItem: Daily,
            newItem: Daily
        ): Boolean {
            return oldItem==newItem
        }

        override fun areContentsTheSame(
            oldItem: Daily,
            newItem: Daily
        ): Boolean {
            return oldItem.date==newItem.date
        }
    }
}