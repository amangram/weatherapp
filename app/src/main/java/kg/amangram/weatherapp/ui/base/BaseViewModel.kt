package kg.amangram.weatherapp.ui.base

import androidx.lifecycle.ViewModel
import kg.amangram.weatherapp.navigation.Screens
import org.koin.java.KoinJavaComponent.inject
import ru.terrakok.cicerone.Router

abstract class BaseViewModel : ViewModel() {
    val screens: Screens by inject(Screens::class.java)
    val router: Router by inject(Router::class.java)
}