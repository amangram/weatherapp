package kg.amangram.weatherapp.ui

import androidx.lifecycle.ViewModel
import kg.amangram.weatherapp.navigation.Screens
import kg.amangram.weatherapp.ui.base.BaseViewModel
import org.koin.java.KoinJavaComponent.inject
import ru.terrakok.cicerone.Router

class MainViewModel:BaseViewModel() {

    fun start(){
        router.replaceScreen(screens.locationsScreen())
    }
}