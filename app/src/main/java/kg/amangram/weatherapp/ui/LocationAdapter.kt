package kg.amangram.weatherapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.model.CurrentWeather
import kg.amangram.weatherapp.databinding.ItemLocationBinding
import kg.amangram.weatherapp.setBg
import kg.amangram.weatherapp.unixToTimeOffset

class LocationAdapter(private val interaction: (Int) -> Unit) :
    ListAdapter<CurrentWeather, LocationAdapter.LocationVH>(LocationDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationVH {
        val inflater = LayoutInflater.from(parent.context)
        return LocationVH(ItemLocationBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: LocationVH, position: Int) {
        holder.bind(getItem(position),position)
    }

    fun swapData(data: List<CurrentWeather>) {
        submitList(data.toMutableList())
    }

    inner class LocationVH(private val itemBinding: ItemLocationBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: CurrentWeather,position: Int) {
            itemBinding.apply {
                tvCity.text = item.name
                tvTemp.text = item.main.temp.toInt().toString() + Constants.CELSIUS
                tvDate.text = unixToTimeOffset(item.timezone)
                bgLocation.setBg(item.weather.first().id)
                root.setOnClickListener {
                    interaction(position)
                }
            }
        }
    }

    private class LocationDC : DiffUtil.ItemCallback<CurrentWeather>() {
        override fun areItemsTheSame(
            oldItem: CurrentWeather,
            newItem: CurrentWeather
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: CurrentWeather,
            newItem: CurrentWeather
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }
}