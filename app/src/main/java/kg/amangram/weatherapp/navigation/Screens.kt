package kg.amangram.weatherapp.navigation

import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.ui.ForecastViewPagerFragment
import kg.amangram.weatherapp.ui.detail.ForecastFragment
import kg.amangram.weatherapp.ui.locations.LocationsFragment
import kg.amangram.weatherapp.ui.search.SearchFragment
import ru.terrakok.cicerone.Screen

class Screens {
    fun forecastScreen(position: Int,list: List<City>): Screen = ForecastViewPagerFragment.ForecastScreen(position,list)

    fun locationsScreen(): Screen = LocationsFragment.LocationsScreen()

    fun searchScreen(): Screen = SearchFragment.SearchScreen()
}