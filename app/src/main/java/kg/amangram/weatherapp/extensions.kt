package kg.amangram.weatherapp

import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.load
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.CurrentTime
import kg.amangram.weatherapp.data.model.WeatherState
import java.text.SimpleDateFormat
import java.time.ZoneOffset
import java.util.*
import kotlin.math.roundToInt


fun Context.showErrorDialog() {
    val positiveButtonClick = { dialog: DialogInterface, which: Int ->
        dialog.cancel()
    }
    val builder = AlertDialog.Builder(this)

    with(builder)
    {
        setTitle("Ошибка")
        setMessage("Отсутствует интернет соединение")
        setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))
        show()
    }
}

fun Long.unixToDay(): String {
    val time = java.util.Date(this * 1000)
    val sdf = SimpleDateFormat("EEEE")
    return sdf.format(time)
}

fun Long.unixTo(): String {
    val time = java.util.Date(this * 1000)
    val sdf = SimpleDateFormat("HH")
    return sdf.format(time)
}

fun Long.unixToHour(timeZone: String): String {
    val time = java.util.Date(this*1000)
    val sdf = SimpleDateFormat("HH")
    sdf.timeZone = TimeZone.getTimeZone(timeZone)
    return sdf.format(time)
}

fun Long.unixToHourInt(timeZone: String): Int {
    val time = java.util.Date(this * 1000)
    val sdf = SimpleDateFormat("HH")
    sdf.timeZone = TimeZone.getTimeZone(timeZone)
    return sdf.format(time).toInt()
}

fun ImageView.setGif(time: Int? = null) {
    val currentBg = if ((getCurrentTime(time)) == CurrentTime.DAY)
        Constants.DAY_BG else Constants.NIGHT_BG
    val imageLoader = ImageLoader.Builder(context)
        .componentRegistry {
            if (Build.VERSION.SDK_INT >= 28) {
                add(ImageDecoderDecoder(context))
            } else {
                add(GifDecoder())
            }
        }
        .build()
    load(currentBg, imageLoader)
}

fun ImageView.setBg(weather: Int) {
    val currentBg = when(weather?.getWeatherState()){
        WeatherState.CLOUDS->resources.getDrawable(R.drawable.bg_cloud,resources.newTheme())
        WeatherState.RAIN->resources.getDrawable(R.drawable.bg_rain,resources.newTheme())
        WeatherState.SNOW->resources.getDrawable(R.drawable.bg_snow,resources.newTheme())
        WeatherState.DRIZZLE->resources.getDrawable(R.drawable.bg_drizzle,resources.newTheme())
        WeatherState.THUNDERSTORM->resources.getDrawable(R.drawable.bg_thunderstorm,resources.newTheme())
        WeatherState.ATMOSPHERE->resources.getDrawable(R.drawable.bg_atmosphere,resources.newTheme())
        else->resources.getDrawable(R.drawable.bg_clear,resources.newTheme())
    }
    val imageLoader = ImageLoader.Builder(context)
        .componentRegistry {
            if (Build.VERSION.SDK_INT >= 28) {
                add(ImageDecoderDecoder(context))
            } else {
                add(GifDecoder())
            }
        }
        .build()
    load(currentBg, imageLoader)
}

fun Long.unixToTime(timeZone: String): String {
    val time = java.util.Date(this * 1000)
    val sdf = SimpleDateFormat("HH:mm")
    sdf.timeZone = TimeZone.getTimeZone(timeZone)
    return sdf.format(time)
}

fun unixToTimeOffset(offset: Int): String{
    val time = java.util.Date(System.currentTimeMillis())
    val sdf = SimpleDateFormat("HH:mm")
    sdf.timeZone = TimeZone.getTimeZone(TimeZone.getAvailableIDs(offset*1000).first())
    return sdf.format(time)
}

fun String.formatIconUrl() = "https://openweathermap.org/img/wn/$this@2x.png"

fun String.formatSize(secondString: String, size: Int): SpannableString {
    val string = "$this\n$secondString"
    val spannable = SpannableString(string)
    spannable.setSpan(
        AbsoluteSizeSpan(size, true),
        0,
        this.length,
        0
    )
    return spannable
}

fun getCurrentTime(time: Int? = null): CurrentTime {
    val calendar = Calendar.getInstance()
    return when (time ?: calendar.get(Calendar.HOUR_OF_DAY)) {
        in 19..23 -> CurrentTime.NIGHT
        in 0..7 -> CurrentTime.NIGHT
        else -> CurrentTime.DAY
    }
}

fun Int.getWeatherState():WeatherState{
    return when(this){
        in 200..232->WeatherState.THUNDERSTORM
        in 300..321->WeatherState.DRIZZLE
        in 500..531->WeatherState.RAIN
        in 600..622->WeatherState.SNOW
        in 700..781->WeatherState.ATMOSPHERE
        800->WeatherState.CLEAR
        else->WeatherState.CLOUDS
    }
}

fun Int.degreesToCardinal(): String {
    val cardinals = arrayOf<String>("N", "NE", "E", "SE", "S", "SW", "W", "NW", "N")
    return cardinals[(this.toDouble() % 360 / 45).roundToInt()]
}

fun Double.formatMaxMin(min: Double) = "Max ${this.toInt()}° min ${min.toInt()}°"

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}
