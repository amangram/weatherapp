package kg.amangram.weatherapp.di

import android.app.Application
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kg.amangram.weatherapp.data.local.CitiesDB
import kg.amangram.weatherapp.data.local.Constants
import kg.amangram.weatherapp.data.model.City
import kg.amangram.weatherapp.data.remote.Api
import kg.amangram.weatherapp.data.repo.ForecastRepo
import kg.amangram.weatherapp.data.repo.LocationRepo
import kg.amangram.weatherapp.navigation.Screens
import kg.amangram.weatherapp.ui.MainViewModel
import kg.amangram.weatherapp.ui.detail.ForecastViewModel
import kg.amangram.weatherapp.ui.locations.LocationsViewModel
import kg.amangram.weatherapp.ui.search.SearchViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.koin.dsl.single
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit

val appModule = module {

    //cicerone
    single { Cicerone.create() }
    single { get<Cicerone<Router>>().router }
    single { get<Cicerone<Router>>().navigatorHolder }
    single<Screens>()

    //db
    single { provideDB(androidApplication()) }
    single { provideDAO(get()) }

    single { ForecastRepo(get()) }
    single { LocationRepo(get(), get()) }

    viewModel { (city: City) -> ForecastViewModel(city, get()) }
    viewModel { LocationsViewModel(get()) }
    viewModel { SearchViewModel(get(),get()) }
    viewModel { MainViewModel() }

    //network
    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }
    single { httpLoggingInterceptor() }
    single { interceptor() }
    single { okHttpClient(get(), get()) }
    single { retrofit(get(),get()) }
    single { getApiService(get()) }
}

fun provideDB(application: Application): CitiesDB {
    return Room.databaseBuilder(application, CitiesDB::class.java, "cities").build()
}

fun provideDAO(db: CitiesDB) = db.getSaved()

fun httpLoggingInterceptor(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.HEADERS
    logging.level = HttpLoggingInterceptor.Level.BODY
    return logging
}

fun interceptor(): Interceptor {
    return Interceptor { chain ->
        val request = chain.request()
        val httpUrl = request.url
        val newRequest = httpUrl.newBuilder()
            .addQueryParameter(Constants.API_KEY_QUERY, Constants.API_KEY)
            .build()
        val requestBuilder = request.newBuilder().url(newRequest)

        chain.proceed(requestBuilder.build())
    }
}

fun okHttpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor,
    interceptor: Interceptor
): OkHttpClient {
    return OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .addNetworkInterceptor(interceptor)
        .build()
}

fun getApiService(retrofit: Retrofit): Api {
    return retrofit.create(Api::class.java)
}

fun retrofit(okHttpClient: OkHttpClient,moshi: Moshi): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(Constants.BASE_URL)
        .client(okHttpClient)
        .build()

}